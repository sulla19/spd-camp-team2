CREATE SCHEMA zoo;

CREATE TABLE zoo.animals (
  id          SERIAL       NOT NULL,
  name        VARCHAR(100) NOT NULL,
  description VARCHAR(100) NOT NULL,
  src_img     VARCHAR(100) NOT NULL
);

INSERT INTO zoo.animals(name, description, src_img) VALUES ('dog', 'sefse', 'sef');
INSERT INTO zoo.animals(name, description, src_img) VALUES ('cat', 'sefse', 'sef');
INSERT INTO zoo.animals(name, description, src_img) VALUES ('bird', 'sefse', 'sef');
INSERT INTO zoo.animals(name, description, src_img) VALUES ('fish', 'sefse', 'sef');
INSERT INTO zoo.animals(name, description, src_img) VALUES ('elephant', 'sefse', 'sef');
INSERT INTO zoo.animals(name, description, src_img) VALUES ('dinosaur', 'sefse', 'sef');
INSERT INTO zoo.animals(name, description, src_img) VALUES ('yeti', 'sefse', 'sef');

