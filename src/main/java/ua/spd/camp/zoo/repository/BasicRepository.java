package ua.spd.camp.zoo.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;

public abstract class BasicRepository<T> {
  protected SessionFactory sessionFactory;
  private Class<T> clazz;

  public BasicRepository(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  protected T save(T obj) {
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    session.save(obj);
    session.getTransaction().commit();
    session.close();
    return obj;
  }

  protected boolean remove(Integer id) {
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    T load = session.load(clazz, id);
    session.delete(load);
    session.getTransaction().commit();
    session.close();
    return true;
  }

  protected boolean update(T obj) {
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    session.saveOrUpdate(obj);
    session.getTransaction().commit();
    session.close();
    return true;
  }

  protected T getById(Integer id) {
    Session session = sessionFactory.openSession();
    T result = session.get(clazz, id);
    session.close();
    return result;
  }

  protected List<T> getAll() {
    return (List<T>) sessionFactory.getCurrentSession().createCriteria(clazz).list();
  }
}
