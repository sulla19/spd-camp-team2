package ua.spd.camp.zoo.repository.exception;

import ua.spd.camp.zoo.model.Animal;

public class AnimalRepositoryException extends RuntimeException {
  private String exceptionMessage = "";
  private Animal animal;

  public AnimalRepositoryException(Throwable e, String exceptionMessage, Animal animal) {
    super(e);
    this.exceptionMessage = exceptionMessage;
    this.animal = animal;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public Animal getAnimal() {
    return this.animal;
  }
}
