package ua.spd.camp.zoo.repository.zoo;

import ua.spd.camp.zoo.model.Animal;

import java.util.List;

public interface AnimalRepository {

    Animal saveAnimal(Animal animal);

    boolean removeAnimal(Integer id);

    boolean updateAnimal(Animal animal);

    Animal getAnimal(Integer id);

    List<Animal> getAllAnimals();
}
