package ua.spd.camp.zoo.repository.exception;

import org.springframework.security.core.userdetails.UserDetails;

public class UserRepositoryException extends RuntimeException {
  private String exceptionMessage = "";
  private UserDetails userDetails;

  public UserRepositoryException(Throwable e, String exceptionMessage, UserDetails userDetails) {
    super(e);
    this.exceptionMessage = exceptionMessage;
    this.userDetails = userDetails;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public UserDetails getUserDetails() {
    return userDetails;
  }
}
