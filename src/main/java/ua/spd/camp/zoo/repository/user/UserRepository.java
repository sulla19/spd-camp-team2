package ua.spd.camp.zoo.repository.user;

import org.springframework.security.core.userdetails.UserDetails;
import ua.spd.camp.zoo.model.Animal;
import java.util.List;

public interface UserRepository {

  UserDetails findByUsername(String username);

  UserDetails saveUser(UserDetails user);

  boolean removeUser(Integer id);

  boolean updateUser(UserDetails user);

  UserDetails getUser(Integer id);

  List<UserDetails> getAllUsers();
}
