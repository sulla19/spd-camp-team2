package ua.spd.camp.zoo.repository.zoo;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.spd.camp.zoo.model.Animal;
import ua.spd.camp.zoo.repository.BasicRepository;
import ua.spd.camp.zoo.repository.exception.AnimalRepositoryException;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class AnimalZooRepository extends BasicRepository<Animal> implements AnimalRepository {

    @Autowired
    public AnimalZooRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Animal saveAnimal(Animal animal) {
        try {
            return save(animal);
        } catch (Exception e) {
            throw new AnimalRepositoryException(e, "Sorry, can't save animal", animal);
        }
    }

    @Override
    public boolean removeAnimal(Integer id) {
        try {
            return remove(id);
        } catch (Throwable e) {
            throw new AnimalRepositoryException(e, "Sorry, can't delete animal", null);
        }
    }

    @Override
    public boolean updateAnimal(Animal animal) {
        try {
            return update(animal);
        } catch (Throwable e) {
            throw new AnimalRepositoryException(e, "Sorry, can't update animal", animal);
        }
    }

    @Override
    public Animal getAnimal(Integer id) {
        try {
            return getById(id);
        } catch (Throwable e) {
            throw new AnimalRepositoryException(e, "Sorry, can't get animal with such id", null);
        }
    }

    @Override
    public List<Animal> getAllAnimals() {
        return getAll();
    }
}
