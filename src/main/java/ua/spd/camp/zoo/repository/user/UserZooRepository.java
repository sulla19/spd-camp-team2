package ua.spd.camp.zoo.repository.user;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import ua.spd.camp.zoo.repository.BasicRepository;
import ua.spd.camp.zoo.repository.exception.UserRepositoryException;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class UserZooRepository extends BasicRepository<UserDetails> implements UserRepository {

    @Autowired
    public UserZooRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public UserDetails findByUsername(String username) {
        try {
            Session session = sessionFactory.openSession();
            // TODO
            return (UserDetails) session.createQuery(
                    "SELECT user FROM WHERE email = ?")
                    .setParameter(0, username)
                    .list().get(0);
        } catch (Throwable e) {
            return null;
        }
    }

    @Override
    public UserDetails saveUser(UserDetails user) {
        try {
            return save(user);
        } catch (Throwable e) {
            throw new UserRepositoryException(e, "Sorry, can't save user", user);
        }
    }

    @Override
    public boolean removeUser(Integer id) {
        try {
            return remove(id);
        } catch (Throwable e) {
            throw new UserRepositoryException(e, "Sorry, can't delete user", null);
        }
    }

    @Override
    public boolean updateUser(UserDetails user) {
        try {
            return update(user);
        } catch (Throwable e) {
            throw new UserRepositoryException(e, "Sorry, can't update user", user);
        }
    }

    @Override
    public UserDetails getUser(Integer id) {
        try {
            return getById(id);
        } catch (Throwable e) {
            throw new UserRepositoryException(e, "Sorry, can't get user with such id", null);
        }
    }

    @Override
    public List<UserDetails> getAllUsers() {
        return getAll();
    }
}
