package ua.spd.camp.zoo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "zoo.animals")
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "src_img")
    private String srcImg;
}
