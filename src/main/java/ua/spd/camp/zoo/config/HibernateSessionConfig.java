package ua.spd.camp.zoo.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:db/hibernate.properties")
public class HibernateSessionConfig {

    @Bean
    private static LocalSessionFactoryBean getSessionFactory(
        @Value("${hibernate.entityPackage}") String basePackage,
        Properties hibernateProperties,
        DataSource dataSource) {

        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(dataSource);
        localSessionFactoryBean.setPackagesToScan(basePackage);
        localSessionFactoryBean.setHibernateProperties(hibernateProperties);
        return localSessionFactoryBean;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        org.springframework.orm.hibernate4.HibernateTransactionManager transactionManager =
            new org.springframework.orm.hibernate4.HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }
}
