package ua.spd.camp.zoo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.spd.camp.zoo.model.Animal;
import ua.spd.camp.zoo.repository.zoo.AnimalRepository;

import java.util.List;

@Service
public class ZooServiceImpl implements ZooService {

    private AnimalRepository zooRepository;

    @Autowired
    public ZooServiceImpl(AnimalRepository repository) {
        this.zooRepository = repository;
    }

    @Override
    public Animal saveAnimal(Animal animal) {
        return zooRepository.saveAnimal(animal);
    }

    @Override
    public boolean removeAnimal(Integer id) {
        return zooRepository.removeAnimal(id);
    }

    @Override
    public boolean updateAnimal(Integer id, Animal animal) {
        return zooRepository.updateAnimal(animal);
    }

    @Override
    public Animal getAnimal(Integer id) {
        return zooRepository.getAnimal(id);
    }

    @Override
    public List<Animal> getAllAnimals() {
        return zooRepository.getAllAnimals();
    }
}
