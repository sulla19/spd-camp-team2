package ua.spd.camp.zoo.service;

import ua.spd.camp.zoo.model.Animal;

import java.util.List;

public interface ZooService {

    Animal saveAnimal(Animal animal);

    boolean removeAnimal(Integer id);

    boolean updateAnimal(Integer id, Animal animal);

    Animal getAnimal(Integer id);

    List<Animal> getAllAnimals();
}
