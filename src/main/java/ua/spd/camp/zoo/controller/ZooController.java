package ua.spd.camp.zoo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.spd.camp.zoo.model.Animal;
import ua.spd.camp.zoo.service.ZooService;

@Controller
public class ZooController {

    private ZooService zooService;

    @Autowired
    public ZooController(ZooService zooService) {
        this.zooService = zooService;
    }

    @GetMapping("/animals/{id}")
    public String getAnimal(Model model, @PathVariable Integer id) {
        model.addAttribute("animal", zooService.getAnimal(id));
        return "animal";
    }

    @GetMapping("/animals")
    public String getAllAnimals(Model model) {
        model.addAttribute("animals", zooService.getAllAnimals());
        return "animals";
    }

    @PostMapping("/animals")
    public String saveAnimal(Animal animal) {
        zooService.saveAnimal(animal);
        return "redirect:/animals";
    }

    @PutMapping("/animals/{id}/update")
    public String  updateAnimal(@PathVariable Integer id, Animal animal) {
        zooService.updateAnimal(id, animal);
        return "redirect:/animals";
    }

    @GetMapping("/animals/{id}/delete")
    public String deleteAnimal(@PathVariable Integer id) {
        zooService.removeAnimal(id);
        return "redirect:/animals";
    }
}
