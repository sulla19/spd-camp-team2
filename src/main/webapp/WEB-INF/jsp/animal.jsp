<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

    <table>
        <tr>
            <td>
                    <%--@elvariable id="animal" type="ua.spd.camp.zoo.model.Animal"--%>
                <c:out value="${animal.id}"/></td>
            <td><c:out value="${animal.name}"/></td>
            <td><c:out value="${animal.description}"/></td>
            <td><c:out value="${animal.srcImg}"/></td>

        </tr>
    </table>

</body>
</html>