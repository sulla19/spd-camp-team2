<%--@elvariable id="animal" type="ua.spd.camp.zoo.model.Animal"--%>
<%--@elvariable id="animals" type="java.util.List"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="fresh-table full-color-orange">
                    <div class="toolbar">
                        <button id="alertBtn" class="btn btn-default">Alert</button>
                    </div>

                    <table id="fresh-table" class="table">
                        <thead>
                        <th data-field="id">ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Image</th>
                        </th>
                        </thead>

                        <tbody>
                        <c:forEach items="${animals}" var="animal">
                            <tr>
                                <td><c:out value="${animal.id}"/></td>
                                <td><c:out value="${animal.name}"/></td>
                                <td><c:out value="${animal.description}"/></td>
                                <td><c:out value="${animal.srcImg}"/></td>
                                <td>
                                    <a href="<c:url value="/animal"/>">
                                        <i class="fa fa-heart"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="<c:url value="/animals/${animal.id}"/>">
                                    <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="/animals/${animal.id}/delete">
                                    <i class="fa fa-remove"></i>
                                    </a>
                                </td>

                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
